package main

import "fmt"

type FuLei struct {
	num1 int
	num2 int
}

type JiaFaZiLei struct {
	FuLei
}

func (j *JiaFaZiLei) JiSuan() int {
	return j.num1 + j.num2
}

type JianFaZiLei struct {
	FuLei
}

func (j *JianFaZiLei) JiSuan() int {
	return j.num1 - j.num2
}

type JieKou interface {
	JiSuan() int
}

func DuoTai(jiekou JieKou) (value int) {
	value = jiekou.JiSuan()
	return
}

type GongChang struct {

}

func (gongchang *GongChang) JiSuanJieGuo(num1, num2 int, yunsuanfu string) (value int) {
	// 创建接口变量
	var jiekou JieKou
	switch yunsuanfu {
	case "+":
		// 创建对象
		var jiafa JiaFaZiLei = JiaFaZiLei{FuLei{num1, num2}}
		jiekou = &jiafa
	case "-":
		var jianfa JianFaZiLei = JianFaZiLei{FuLei{num1, num2}}
		jiekou = &jianfa
	}
	value = DuoTai(jiekou)
	return
}


func main()  {
	var gongchang GongChang
	var value int = gongchang.JiSuanJieGuo(10, 20, "+")
	fmt.Println(value)
}
