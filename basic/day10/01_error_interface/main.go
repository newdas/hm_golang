package main

import (
	"errors"
	"fmt"
)

func test(a, b int) (value int, err error) {
	if b == 0 {
		err = errors.New("0不能作为除数")
		return
	} else {
		value = a /b
		return
	}
}

func main() {
	value ,err := test(100, 0)
	if err == nil {
		fmt.Printf("answer is %d\n", value)
	} else {
		fmt.Printf("error is %s\n", err)
	}
}
