package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	fileSrc, _ := os.Open("D:/av/final_secure/video/YK1110.mp4")
	fileDes, _ := os.Create("demo.mp4")

	defer fileDes.Close()
	defer fileSrc.Close()

	b := make([]byte, 1024*1024)

	for {
		n, err := fileSrc.Read(b)
		if err == io.EOF {
			break
		}

		fileDes.Write(b[:n])
	}

	fmt.Println("copy file success.")
}
