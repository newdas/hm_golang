package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	file, err := os.Create("./demo.txt")
	if err != nil {
		fmt.Printf("create file error: %s\n", err)
		return
	}

	defer file.Close()

	fmt.Println("file create success.")

	var str string = "你好中国\r\n"
	n, _ := file.WriteString(str)
	fmt.Printf("写入%q, 共%d个字符\n", str, n)
	// 写入"你好中国\r\n", 共14个字符
	// 每个中文占三个字符

	n,err = file.Write([]byte{'h', 'h'})
	if err != nil {
		fmt.Printf("write to file error: %s\n", err)
	} else {
		fmt.Printf("写入hh， 共%d个字符\n", n)
	}

	file.Seek(-2, io.SeekEnd)
	file.WriteString("p")
}
