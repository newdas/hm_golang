package main

import "fmt"

func demo(i int)  {
	defer func() {
		err := recover()
		if err != nil {
			fmt.Printf("recover error: %s\n", err)
		}
	}()

	//var p *int
	//*p = 100
	// recover error: runtime error: invalid memory address or nil pointer dereference


	var arr [10]int
	arr[i] =100
	// recover error: runtime error: index out of range


}
func main() {
	demo(100)
}
