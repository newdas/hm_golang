package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func main01() {
	file, err := os.Open("./demo.txt")
	if err != nil {
		fmt.Printf("Open file error: %s\n", err)
	}

	b := make([]byte, 1024)
	n, err := file.Read(b)
	if err !=nil {
		fmt.Printf("reaf file to byte slice error: %s\n", err)
	}

	fmt.Println(string(b[:n]))
}

func main02()  {
	file, err := os.Open("./demo.txt")
	if err != nil {
		fmt.Printf("Open file error: %s\n", err)
	}

	// 创建缓冲区
	r := bufio.NewReader(file)
	b, _ := r.ReadBytes('\n')
	fmt.Println(string(b))
	b, _ = r.ReadBytes('\n')
	fmt.Println(string(b))
}

func main() {
	file, err := os.Open("./demo.txt")
	if err != nil {
		fmt.Printf("Open file error: %s\n", err)
	}
	defer file.Close()

	b := make([]byte, 10)
	for {
		n, err := file.Read(b)
		if err == io.EOF{
			break
		}

		fmt.Println((b[:n]))
	}
}
