package main

import (
	"fmt"
	"os"
)

func main() {
	file, err := os.Create("./demo.txt")
	if err != nil {
		fmt.Printf("create file error: %s\n", err)
		return
	}

	defer file.Close()

	fmt.Println("file create success.")
}
