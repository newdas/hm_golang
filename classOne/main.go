package main

import (
	_ "my_git_repos/hm_golang/classOne/routers"
	_ "my_git_repos/hm_golang/classOne/models"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}

