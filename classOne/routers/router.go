package routers

import (

	"my_git_repos/hm_golang/classOne/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.MainController{})
    beego.Router("/index/?:id/", &controllers.IndexController{}, "get:Post;post:Get")
    beego.Router("/insert/", &controllers.MysqlController{}, "get:Insert")
    beego.Router("/query/", &controllers.MysqlController{}, "get:Query")
}
