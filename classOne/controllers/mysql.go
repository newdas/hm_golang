package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
	"my_git_repos/hm_golang/classOne/models"
)

type MysqlController struct {
	beego.Controller
}

func (this *MysqlController) Insert() {
	err := models.CreateUser(100, "golang")
	if err != nil {
		this.Ctx.WriteString("insert error.")
	} else {
		this.Ctx.WriteString("insert success.")
	}
}

func (this *MysqlController) Query() {
	users, err := models.QueryUser()
	if err != nil {
		this.Ctx.WriteString("query error.")
	}
	this.Ctx.WriteString(fmt.Sprintf("%#v", users))
}