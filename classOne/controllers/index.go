package controllers

import (
	"github.com/astaxie/beego"
)

type IndexController struct {
	beego.Controller
}

func (c *IndexController) Get() {
	id := c.GetString(":id")
	beego.Info("id =", id)
	c.Data["id"] = id
	c.Data["title"] = "index get"
	c.TplName = "test.html"
}

func (c *IndexController) Post() {
	c.Data["title"] = "index post"
	c.TplName = "test.html"
}
