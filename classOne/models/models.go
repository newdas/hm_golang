package models

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

type User struct {
	Id   int
	Name string
}

func init() {
	err := orm.RegisterDataBase("default", "mysql", "root:123456@tcp(127.0.0.1:3306)/class?charset=utf8")
	if err != nil {
		panic(err)
	}

	orm.RegisterModel(new(User))

	err = orm.RunSyncdb("default", false, true)
	if err != nil {
		panic(err)
	}
}

func CreateUser(id int, name string) error {
	o := orm.NewOrm()

	user := User{id, name}
	_, err := o.Insert(&user)
	if err != nil {
		beego.Info("insert err:", err)
		return err
	}

	beego.Info(fmt.Sprintf("create %v success.", user))
	return nil
}

func QueryUser() (*[]User, error) {
	o := orm.NewOrm()

	users := new([]User)
	_, err := o.QueryTable("user").All(users)
	if err != nil {
		beego.Info("query err:", err)
		return nil, err
	}
	return users, nil
}
