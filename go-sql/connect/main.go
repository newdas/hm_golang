package main

import (
	"database/sql"
	"fmt"
)
import _ "github.com/go-sql-driver/mysql"

const sqlType = "mysql"
// username:password@protocol(address)/dbname?param=value
const dataSourceName = "root:123456@(127.0.0.1:3306)/zgpingshu"

func main() {
	// sql open
	db, err:= sql.Open(sqlType, dataSourceName)
	if err != nil {
		fmt.Println("sql open err:", err)
		return
	}
	defer db.Close()

	// ping
	err = db.Ping()
	if err != nil {
		fmt.Println("db ping err:", err)
		return
	}

	fmt.Println("db connect success.")
}
