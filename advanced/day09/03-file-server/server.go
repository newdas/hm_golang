package main

import (
	"io"
	"net/http"
	"os"
)

const FILE_PATH = "D:/test"

func sendFile(filePath string, writer http.ResponseWriter) {
	file, err := os.Open(filePath)
	if err != nil {
		writer.Write([]byte(err.Error()))
	} else {
		buf := make([]byte, 4096)
		for {
			n, err := file.Read(buf)
			if err == io.EOF {
				break
			}
			writer.Write(buf[:n])
		}
	}
}
func handleFunc(writer http.ResponseWriter, request *http.Request) {
	filePath := FILE_PATH + request.URL.String()
	sendFile(filePath, writer)
}

func main() {
	http.HandleFunc("/", handleFunc)

	http.ListenAndServe(":9999", nil)
}
