package main

import (
	"io"
	"net/http"
)

func main() {
	http.HandleFunc("/hello/", func(writer http.ResponseWriter, request *http.Request) {
		io.WriteString(writer, "<h1>hello are you ok?</h1>")
	})

	http.ListenAndServe(":8888", nil)
}
