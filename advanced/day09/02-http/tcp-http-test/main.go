package main

import (
	"fmt"
	"net"
	"os"
)

func ErrFunc(tag string, err error) {
	if err != nil {
		fmt.Println(tag, err)
		os.Exit(1)
	}
}

func main() {
	listener, err := net.Listen("tcp", "127.0.0.1:8889")
	ErrFunc("listen err:", err)
	defer listener.Close()

	for {
		conn, err := listener.Accept()
		ErrFunc("accept err: ", err)
		defer conn.Close()

		conn.Read(nil)
		conn.Write([]byte("HTTP/1.1 200 OK\r\nDate: Wed, 07 Nov 2018 14:26:21 GMT\r\nContent-Length: 26\r\nContent-Type: text/html; charset=utf-8\r\n\r\n<h1>hello are you ok?</h1>"))
	}

}
