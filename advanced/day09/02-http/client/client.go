package main

import (
	"fmt"
	"net"
	"os"
)

func ErrFunc(tag string, err error) {
	if err != nil {
		fmt.Println(tag, err)
		os.Exit(1)
	}
}

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:8888")
	ErrFunc("dial err:", err)
	defer conn.Close()

	request := "GET /hello HTTP/1.1\r\nHost:127.0.0.1\r\n\r\n"
	_, err = conn.Write([]byte(request))
	ErrFunc("Write", err)

	buf := make([]byte, 4096)
	n, err := conn.Read(buf)
	ErrFunc("read err: ", err)
	fmt.Printf("%q", string(buf[:n]))
	// HTTP/1.1 200 OK\r\nDate: Wed, 07 Nov 2018 14:26:21 GMT\r\nContent-Length: 26\r\nContent-Type: text/html; charset=utf-8\r\n\r\n<h1>hello are you ok?</h1>
}
