package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

const URL = "http://127.0.0.1:9999/test.mp4"
func main() {
	resp, err := http.Get(URL)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("header: ", resp.Header)
	fmt.Println("status code: ", resp.StatusCode)
	fmt.Println("proto: ", resp.Proto)

	res, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(res))
}
