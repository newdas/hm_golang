package main

import (
	"fmt"
	"net"
	"os"
)

func ErrFunc(tag string, err error) {
	if err != nil {
		fmt.Println(tag, err)
		os.Exit(1)
	}
}
func main() {
	listener, err := net.Listen("tcp", "127.0.0.1:8000")
	ErrFunc("net listen err: ", err)
	defer listener.Close()

	conn, err := listener.Accept()
	ErrFunc("accept err: ", err)
	defer conn.Close()

	buf := make([]byte, 40960)
	n, err := conn.Read(buf)
	ErrFunc("read err: ", err)
	if n == 0 {
		return
	}
	fmt.Printf("%q",string(buf[:n]))

	/*
	请求行：请求方法 请求URL 协议版本
	请求头
	空行 \r\n 代表http请求结束
	请求包体
	 */

	/*
	GET / HTTP/1.1
	Host: 127.0.0.1:8000
	Connection: keep-alive
	Cache-Control: max-age=0
	Upgrade-Insecure-Requests: 1
	User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36
	Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,**;q=0.8
	Accept-Encoding: gzip, deflate, br
	Accept-Language: zh-CN,zh;q=0.9
	Cookie: csrftoken=3XUJwacGotDMhIbNeNfrEFSUE30XqaE4mrOPaf5T2rTMQGGU22Av34oJg0s4hSiI


	*/
}
