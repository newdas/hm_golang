package main

import (
	"fmt"
	"net"
	"time"
)

func main() {
	// 组织一个udp地址结构， 指定服务器的ip和端口
	addr, err := net.ResolveUDPAddr("udp", "127.0.0.1:9998")
	if err != nil {
		fmt.Println("ResolveUDPAddr error: ", err)
		return
	}

	// 创建用户通信的socket
	udpConn, err := net.ListenUDP("udp", addr)
	if err != nil {
		fmt.Println("ListenUDP error: ", err)
		return
	}
	defer udpConn.Close()
	fmt.Println("udp server created success.")

	// 读取客户端发送的数据
	buf := make([]byte, 4096)

	for {
		n, clientAddr, err := udpConn.ReadFromUDP(buf)
		if err != nil {
			fmt.Println("ReadFromUDP error: ", err)
			return
		}

		fmt.Println(clientAddr.String(), "connected to server.")

		// 服务器读取到数据
		fmt.Printf("read from %s, data: %s\n", clientAddr.String(), buf[:n])

		go func() {
			// 回写数据给客户端
			_, err = udpConn.WriteToUDP([]byte(time.Now().String()), clientAddr)
			if err != nil {
				fmt.Println("WriteToUDP error: ", err)
				return
			}
		}()
	}

}
