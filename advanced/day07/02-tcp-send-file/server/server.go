package main

import (
	"fmt"
	"net"
	"os"
)

func recvFile(conn net.Conn, fileName string)  {
	file, err := os.Create(fileName)
	if err != nil {
		fmt.Println("create file error: ", err)
		return
	}

	buf := make([]byte, 4096)
	for {
		n, _ := conn.Read(buf)
		if n == 0 {
			fmt.Println("recv file complete, quit.")
			return
		}
		file.Write(buf[:n])
	}
}

func main() {
	// 创建用于监听的socket
	listener, err := net.Listen("tcp", "127.0.0.1:8088")
	if err != nil {
		fmt.Println("net listen error: ", err)
		return
	}
	defer listener.Close()

	// 阻塞监听客户端链接
	fmt.Println("wait for client to connect")
	conn, err := listener.Accept()
	if err != nil {
		fmt.Println("listener accept error: ", err)
		return
	}
	defer conn.Close()

	// 读取文件名
	buf := make([]byte, 4096)
	n, err := conn.Read(buf)
	if err != nil {
		fmt.Println("conn read error: ", err)
		return
	}
	fileName := string(buf[:n])

	// 回客户端ok
	conn.Write([]byte("ok"))

	// 获取文件内容
	recvFile(conn, fileName)
}
