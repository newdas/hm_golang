package main

import (
	"fmt"
	"io"
	"net"
	"os"
)

func sendFile(conn net.Conn, filePath string)  {
	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println("open file err: ", err)
		return
	}

	buf := make([]byte, 4096)
	for {
		n, err := file.Read(buf)
		if err != nil {
			if err == io.EOF {
				fmt.Println("send file complete.")
				return
			} else {
				fmt.Println("file read err: ", err)
				return
			}
		}
		_, err = conn.Write(buf[:n])
		if err != nil {
			fmt.Println("conn write err: ", err)
			return
		}
	}
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("args err, quit!")
		return
	}

	conn, err := net.Dial("tcp", "127.0.0.1:8088")
	if err != nil {
		fmt.Println("connect to server err: ", err)
		return
	}
	defer conn.Close()

	filePath := os.Args[1]
	fileInfo, err := os.Stat(filePath)
	if err != nil {
		fmt.Println("check file info err: ", err)
		return
	}

	fileName := fileInfo.Name()

	conn.Write([]byte(fileName))

	buf := make([]byte, 4096)
	n, err := conn.Read(buf)
	if err != nil {
		fmt.Println("conn read err: ", err)
		return
	}
	if "ok" == string(buf[:n]) {
		sendFile(conn, filePath)
	} else {
		fmt.Println("not ok, quit.")
	}
}
