package main

import (
	"fmt"
)

func produce(ch chan<- int, id int)  {
	for i:=0; i< 10; i++ {
		fmt.Printf("producer(%02d) produce %02d\n", id, i)
		ch <- i
	}
	close(ch)
}

func consume(ch <-chan int, id int)  {
	for i := range ch {
		fmt.Printf("consumer(%02d) consume %02d\n", id, i)
	}
}

func main() {
	ch := make(chan int)

	go produce(ch, 1)

	for i:=0; i<2; i++ {
		go consume(ch, i)
	}

	for {
		;
	}
}
