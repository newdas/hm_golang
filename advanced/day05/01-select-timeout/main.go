package main

import (
	"fmt"
	"time"
)

func main() {
	dataCh := make(chan int)
	quitCh := make(chan struct{})

	go func() {
		for t := range time.NewTicker(time.Second).C {
			fmt.Println(t)
		}
	}()
	go func() {
		for {
			select {
			case data := <- dataCh:
				fmt.Println(data)
			case <- time.After(time.Second * 5):
				fmt.Println("time out")
				quitCh <- struct{}{}
				return
			}
		}
	}()
	go func() {
		time.Sleep(time.Second * 3)
		dataCh <- 100
	}()
	<- quitCh

	/*
	2018-10-18 20:35:54.7388717 +0800 CST m=+1.005164401
2018-10-18 20:35:55.7396517 +0800 CST m=+2.005944401
2018-10-18 20:35:56.7389344 +0800 CST m=+3.005227101
100
2018-10-18 20:35:57.7400451 +0800 CST m=+4.006337701
2018-10-18 20:35:58.7401189 +0800 CST m=+5.006411501
2018-10-18 20:35:59.7398513 +0800 CST m=+6.006144001
2018-10-18 20:36:00.7386731 +0800 CST m=+7.004965701
2018-10-18 20:36:01.7387149 +0800 CST m=+8.005007501
time out
	 */
}
