package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const NUM = 3
var cond sync.Cond

// 生产者
func producer(in chan<- int, idx int)  {
	for {
		cond.L.Lock() // 条件变量对应互斥锁加锁
		for len(in) == 3 {  //  产品区满了
			cond.Wait()  // 挂起当前goroutine，等待条件变量满足时，被消费者唤醒
		}
		data := rand.Intn(1000) // 生产一个随机数
		in <- data // 写入到channel中
		fmt.Printf("%d号生产者，产生数据%03d，公共区剩余%d个数据\n", idx, data, len(in))
		cond.L.Unlock() // 生产结束，解锁互斥锁
		cond.Signal()  // 唤醒阻塞的消费者
		time.Sleep(time.Second) // 生产完休息一会，给其他goroutine执行的机会
	}
}

// 消费者
func consumer(out <-chan int, idx int)  {
	for {
		cond.L.Lock() // 条件变量对应互斥锁加锁
		for len(out) == 0 {  //  产品区满了
			cond.Wait()  // 挂起当前goroutine，等待条件变量满足时，被消费者唤醒
		}
		data :=  <- out // 写入到channel中
		fmt.Printf("%d号消费者者，消费数据%03d，公共区剩余%d个数据\n", idx, data, len(out))
		cond.L.Unlock() // 生产结束，解锁互斥锁
		cond.Signal()  // 唤醒阻塞的消费者
		time.Sleep(time.Second) // 生产完休息一会，给其他goroutine执行的机会
	}
}
func main() {
	ch := make(chan int, 3)
	cond.L = new(sync.Mutex)
	for i:=0; i<2;i++ {
		go producer(ch, i)
	}
	for i:=0; i<2;i++ {
		go consumer(ch, i)
	}
	for {
		;
	}
}
