package main

import (
	"fmt"
	"runtime"
	"time"
)

func main() {
	dataCh := make(chan int)
	quitCh := make(chan bool)

	go func() {  // write data
		for i:=0; i<5; i++ {
			time.Sleep(time.Second)
			dataCh <- i
		}
		quitCh <- true
		fmt.Println("enough time to print me!")
		close(dataCh)
		runtime.Goexit()
	}()

	loop: for {
		select {
		case data := <-dataCh:
			fmt.Println("receive data: ", data)
		case <- quitCh:
			fmt.Println("quit: receive quit signal!!!!")
			break loop
		}
	}

}
