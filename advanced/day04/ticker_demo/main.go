package main

import (
	"fmt"
	"time"
)

func main() {
	myTicker := time.NewTicker(time.Second * 2)
	fmt.Println(time.Now())

	quit := make(chan struct{})

	count := 0
	go func(count int) {
		for t := range myTicker.C {
			fmt.Println(t)
			count++
			if count == 5 {
				quit <- struct{}{}
			}
		}
	}(count)

	<- quit

	/*
	2018-10-17 00:17:36.6826974 +0800 CST m=+0.003968401
	2018-10-17 00:17:38.6827925 +0800 CST m=+2.004063501
	2018-10-17 00:17:40.6841272 +0800 CST m=+4.005398201
	2018-10-17 00:17:42.6824921 +0800 CST m=+6.003763101
	2018-10-17 00:17:44.6833316 +0800 CST m=+8.004602701
	2018-10-17 00:17:46.6824725 +0800 CST m=+10.003743501
	 */
}
