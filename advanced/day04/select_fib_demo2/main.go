package main

import "fmt"

func fib(dataCh chan<- int, quitCh chan<- bool)  {
	x, y := 1, 1
	for i:=0; i<10; i++ {
		dataCh <- x
		x, y = y, x+y
	}
	quitCh <- true
	close(dataCh)

}

func main() {
	dataCh := make(chan int)
	quitCh := make(chan bool)

	go fib(dataCh, quitCh)

	loop: for {
		select {
		 case i:= <-dataCh:
		 	fmt.Println(i)
		 case <-quitCh:
		 	fmt.Println("quit")
		 	break loop
		}
	}

}
