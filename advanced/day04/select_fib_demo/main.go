package main

import (
	"fmt"
	"time"
)

func main() {
	start := 1
	fmt.Println(start)
	dataCh := make(chan int, 1)

	go func() {
		dataCh <- 1
	}()

	for data := range dataCh {
		time.Sleep(time.Second)
		fmt.Println(data)
		dataCh <- start + data
		start = data
	}
}
