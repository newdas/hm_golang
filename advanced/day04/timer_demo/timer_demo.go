package main

import (
	"fmt"
	"time"
)

func main()  {
	myTimer := time.NewTimer(time.Second * 2)
	fmt.Println(time.Now())
	// 每2s写入系统当前时间
	fmt.Println(<-myTimer.C)
	fmt.Println(<-time.After(time.Second * 2))
	/*
	2018-10-17 00:06:40.7307805 +0800 CST m=+0.020334201
	2018-10-17 00:06:42.7360956 +0800 CST m=+2.025649301
	2018-10-17 00:06:44.7365752 +0800 CST m=+4.026128901
	 */
}