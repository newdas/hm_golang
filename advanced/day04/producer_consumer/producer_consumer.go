package producer_consumer

import "fmt"

func producer(inCh chan<- int) {
	for i := 0; i < 10; i++ {
		fmt.Println("produce: ", i)
		inCh <- i * i
	}
	close(inCh)
}

func consumer(outCh <-chan int) {
	for i := range outCh {
		fmt.Println("consume: ", i)
	}
}

func main() {
	ch := make(chan int, 5)
	go producer(ch)
	consumer(ch)
}
