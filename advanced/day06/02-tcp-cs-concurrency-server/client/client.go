package main

import (
	"fmt"
	"net"
	"os"
)

func main() {
	conn, err := net.Dial("tcp", "192.168.1.33:8088")
	if err != nil {
		fmt.Println("connect to server err: ", err)
		return
	}
	defer conn.Close()

	go func() {

		buf := make([]byte, 1024)
		for {
			n, err := os.Stdin.Read(buf)
			if err != nil {
				fmt.Println("read stdin error: ", err)
				return
			}
			conn.Write(buf[:n])
		}

	}()

	readBuf := make([]byte, 1024)
	for {
		n, err := conn.Read(readBuf)
		if n == 0 {
			fmt.Println("server closed, quit.")
			return
		}
		if err != nil {
			fmt.Println("receive data  error: ", err)
			return
		}
		fmt.Println("receive data: ", string(readBuf[:n]))

	}
}
