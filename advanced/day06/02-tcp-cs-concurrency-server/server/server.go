package main

import (
	"fmt"
	"io"
	"net"
)

func HandlerConn(conn net.Conn) {
	defer conn.Close()

	addr := conn.RemoteAddr().String()
	fmt.Println(addr, "client connect success.")

	// read data
	buf := make([]byte, 1024)

	for {
		n, err := conn.Read(buf)
		if n == 0 {
			fmt.Println("connect closed.")
			return
		}
		if err != nil {
			fmt.Println("read data error: ", err)
			return
		}
		if string(buf[:n]) == "exit\n" || string(buf[:n]) == "exit\r\n" {
			fmt.Println("receive quit signal, close ", addr)
			return
		}
		fmt.Printf("%s: %s", addr, buf[:n])
		io.WriteString(conn, "i receive your data\n")
	}

}
func main() {
	listener, err := net.Listen("tcp", ":8088")
	if err != nil {
		fmt.Println("net listen error: ", err)
		return
	}
	defer listener.Close()

	fmt.Println("wait for client to connect")

	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("listener accept error: ", err)
			return
		}
		go HandlerConn(conn)
	}

}
