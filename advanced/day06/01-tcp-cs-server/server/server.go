package main

import (
	"fmt"
	"net"
)

func main() {
	listener, err := net.Listen("tcp", "127.0.0.1:8088")
	if err != nil {
		fmt.Println("net listen error: ", err)
		return
	}
	defer listener.Close()

	fmt.Println("wait for client to connect")
	conn, err := listener.Accept()
	if err != nil {
		fmt.Println("listener accept error: ", err)
		return
	}
	defer conn.Close()

	fmt.Println("client connect success.")

	// read data
	buf := make([]byte, 1024)
	n, err := conn.Read(buf)
	if err !=nil {
		fmt.Println("read data error: ", err)
		return
	}

	fmt.Println("server received data: ", string(buf[:n]))
}
