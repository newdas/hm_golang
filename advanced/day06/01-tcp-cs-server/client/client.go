package main

import (
	"fmt"
	"net"
)

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:8088")
	if err != nil {
		fmt.Println("connect to server err: ", err)
		return
	}
	defer conn.Close()

	conn.Write([]byte("刘达， 你好"))

}
