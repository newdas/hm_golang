package main

import (
	"fmt"
	"github.com/pkg/errors"
	"io"
	"net/http"
	"strconv"
)


func getPageUrl(page int) string {
	return "https://movie.douban.com/top250?start=" + strconv.Itoa(25 * (page -1)) + "&filter="
}

func crawlUrl(url string) (result string, err error) {
	// check http get
	resp, err := http.Get(url)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	// check status code
	if resp.StatusCode != http.StatusOK {
		err = errors.New("status code " + strconv.Itoa(resp.StatusCode))
		return
	}

	// read response body
	buf := make([]byte, 4096)
	for {
		// done
		n, err1 := resp.Body.Read(buf)
		if n == 0 {
			break
		}
		// error occur
		if err1 != nil && err1 != io.EOF {
			result = ""
			err = err1
			return
		}
		result += string(buf[:n])
	}
	return
}

func parseContent(page int, pageContent string)  {
	content := NewContentFromPageContent(page, pageContent)
	content.SavePage()
}

func crawlPage(page int, url string, ch chan<- struct{}) {
	fmt.Printf("crawling page %d\n", page)

	content, err := crawlUrl(url)

	if err != nil {
		fmt.Printf("crawl page %d err: %v\n", page, err)
		return
	}
	parseContent(page, content)

	ch <- struct{}{}
}

func startCrawl(start, end int) {
	fmt.Printf("crawling page %d to page %d\n", start, end)

	// channel to async
	ch := make(chan struct{}, 10)
	for i := start; i <= end; i++ {
		url := getPageUrl(i)
		go crawlPage(i, url, ch)
	}

	// read channel block to wait
	for i := start; i < end; i++ {
		<-ch
	}

}

func main() {
	var start, end int
	// start page
	fmt.Print("please enter start page(>=1):")
	fmt.Scan(&start)

	// end page
	fmt.Print("please enter end page(>=start):")
	fmt.Scan(&end)

	// timer
	timer := NewTimer()

	// start crawl
	startCrawl(start, end)

	// timer tick
	timer.tick()
}
