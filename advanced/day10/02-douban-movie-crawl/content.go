package main

import (
	"fmt"
	"os"
	"regexp"
)

var reTitle = regexp.MustCompile(`<img width="100" alt="(?s:(.*?))"`)
var reRate = regexp.MustCompile(`<span class="rating_num" property="v:average">(?s:(.*?))</span>`)
var reScoreNumber = regexp.MustCompile(`<span>(?s:(\d*?))人评价</span>`)

type Content struct {
	page            int
	titleList       [][]string
	rateList        [][]string
	scoreNumberList [][]string
}

func (content *Content) SavePage() {
	fileName := fmt.Sprintf("page%03d.csv", content.page)
	file, err := os.Create(fileName)
	if err != nil {
		fmt.Println("create file err:", err)
		return
	}

	file.WriteString("title,rate,scoreNumber\n")
	length := len(content.titleList)
	for i := 0; i < length; i++ {
		file.WriteString(content.titleList[i][1] + "," + content.rateList[i][1] + "," + content.scoreNumberList[i][1] + "\n")
	}

	file.Close()
}
func NewContent(page int, titleList, rateList, scoreNumberList [][]string) *Content {
	content := &Content{
		page:            page,
		titleList:       titleList,
		rateList:        rateList,
		scoreNumberList: scoreNumberList,
	}
	return content
}

func NewContentFromPageContent(page int, pageContent string) *Content {
	return NewContent(
		page,
		reTitle.FindAllStringSubmatch(pageContent, -1),
		reRate.FindAllStringSubmatch(pageContent, -1),
		reScoreNumber.FindAllStringSubmatch(pageContent, -1),
	)
}
