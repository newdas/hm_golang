package main

import (
	"fmt"
	"time"
)

type Timer struct {
	t time.Time
}

func NewTimer() *Timer {
	timer := new(Timer)
	timer.start()
	return timer
}

func (timer *Timer) start() {
	timer.t = time.Now()
}

func (timer *Timer) tick() {
	timeNow := time.Now()
	fmt.Printf("time use: %s\n", timeNow.Sub(timer.t))
	timer.t = timeNow
}