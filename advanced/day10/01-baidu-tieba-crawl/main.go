package main

import (
	"fmt"
	"github.com/pkg/errors"
	"io"
	"net/http"
	"os"
	"strconv"
)

const UrlP = "https://tieba.baidu.com/f?kw=%E7%88%86%E7%85%A7&ie=utf-8&pn="

func crawlUrl(url string) (result string, err error) {
	// check http get
	resp, err := http.Get(url)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	// check status code
	if resp.StatusCode != http.StatusOK {
		err = errors.New("status code " + strconv.Itoa(resp.StatusCode))
		return
	}

	// read response body
	buf := make([]byte, 4096)
	for {
		// done
		n, err1 := resp.Body.Read(buf)
		if n == 0 {
			break
		}
		// error occur
		if err1 != nil && err1 != io.EOF {
			result = ""
			err = err1
			return
		}
		result += string(buf[:n])
	}
	return
}

func crawlPage(page int, url string) {
	fmt.Printf("crawling page %d\n", page)

	content, err := crawlUrl(url)

	if err != nil {
		fmt.Printf("crawl page %d err: %v\n", page, err)
	} else {
		fmt.Printf("crawl page %d success: %s\n", page, content[:10])
	}
}

func savePage(page int, url string, ch chan<- struct{}) {
	fmt.Printf("saving page %d\n", page)

	fileName := fmt.Sprintf("page%03d.html", page)

	resp, err := http.Get(url)
	if err != nil {
		fmt.Printf("http get page %d err: %v\n", page, err)
		return
	}

	file, err := os.Create(fileName)
	if err != nil {
		fmt.Printf("create file %s err: %v\n", fileName, err)
		return
	}
	defer file.Close()

	io.Copy(file, resp.Body)

	ch <- struct{}{}
}

func startCrawl(start, end int) {
	fmt.Printf("crawling page %d to page %d\n", start, end)

	// channel to async
	ch := make(chan struct{}, 10)
	for i := start; i <= end; i++ {
		url := UrlP + strconv.Itoa((i-1)*50)
		crawlPage(i, url)
		go savePage(i, url, ch)
	}

	// read channel block to wait
	for i := start; i < end; i++ {
		<-ch
	}

}

func main() {
	var start, end int
	// start page
	fmt.Print("please enter start page(>=1):")
	fmt.Scan(&start)

	// end page
	fmt.Print("please enter end page(>=start):")
	fmt.Scan(&end)

	// timer
	timer := NewTimer()

	// start crawl
	startCrawl(start, end)

	// timer tick
	timer.tick()
}
