package main

import (
	"fmt"
	"runtime"
)

func main() {
	// 返回执行之前使用的cpu核数
	fmt.Println(runtime.GOMAXPROCS(1)) // 4
	fmt.Println(runtime.GOMAXPROCS(2)) // 1
	fmt.Println(runtime.NumCPU()) // 4
}
