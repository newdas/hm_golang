package main

import (
	"fmt"
	"os"
)

func main() {
	// 获取用户输入的目录路径
	fmt.Println("请输入待查询的目录")
	var path string
	fmt.Scan(&path)

	// 打开目录
	file, err := os.OpenFile(path, os.O_RDONLY, os.ModeDir)
	if err != nil {
		fmt.Println("OpenFile err:", err)
		return
	}
	defer file.Close()

	// 读取目录项
	infos, err := file.Readdir(-1) // -1 读取所有
	for _, fileInfo := range infos {
		if fileInfo.IsDir() {
			fmt.Println(fileInfo.Name(), "是目录")
		} else {
			fmt.Println(fileInfo.Name(), "是文件")
		}
	}
}
