package main

import (
	"fmt"
	"net"
	"strings"
	"time"
)

//创建用户结构体类型
type Client struct {
	C    chan string
	Name string
	Addr string
}

// 创建全局map， 存储在线用户
var onlineMap map[string]Client

// 创建全局channel，传递用户消息
var message = make(chan string)

func WriteMsgToClient(client Client, conn net.Conn) {
	// 监听用户自带channel上是否有消息
	for msg := range client.C {
		conn.Write([]byte(msg + "\n"))
	}
}

func MakeMsg(client Client, msg string) (buf string) {
	buf = "[" + client.Addr + "--" + client.Name + "]:" + msg
	return
}
func HandleConnect(conn net.Conn) {
	defer conn.Close()

	// 获取用户网络地址
	addr := conn.RemoteAddr().String()
	// 创建新连接用户的结构体信息
	client := Client{C: make(chan string), Name: addr, Addr: addr}
	// 将新连接用户添加到在线用户map中, key:ip+port ,value:client
	onlineMap[addr] = client

	// 创建专门用来给当前用户发消息的goroutine
	go WriteMsgToClient(client, conn)

	// 发送用户上线消息到全局通道中
	message <- MakeMsg(client, "login")

	// 检测客户端退出的channel
	quitChan := make(chan struct{}, 0)

	// 判断用户是否活跃的channel
	activeChan := make(chan struct{}, 0)
	// 创建一个goroutine，专门处理用户发送的消息
	go func() {
		buf := make([]byte, 4096)
		for {
			// 用户活跃
			activeChan <- struct{}{}

			n, err := conn.Read(buf)
			if n == 0 {
				fmt.Printf("检测到客户端%s退出\n", client.Name)
				quitChan <- struct{}{}
				return
			}
			if err != nil {
				fmt.Println("conn read err: ", err)
				return
			}

			// 读到的消息广播出去
			msg := string(buf[:n])
			msg = strings.TrimSpace(msg)
			if msg == "who" || msg == "who\n" || msg == "msg\r\n" {
				conn.Write([]byte("online user list: \n"))
				for _, _client := range onlineMap {
					conn.Write([]byte("\t" + _client.Name + "\n"))
				}
			} else if len(msg) > 7 && msg[:7] == "rename|" { // rename|new_name
				newName := strings.Split(msg, "|")[1]
				client.Name = newName
				conn.Write([]byte("rename success.\n"))
			} else {
				message <- MakeMsg(client, msg)
			}
		}
	}()
	for {
		select {
		case <-quitChan:
			delete(onlineMap, client.Addr)
			message <- MakeMsg(client, "logout")
			return
		case <-time.After(10 * time.Second):
			delete(onlineMap, client.Addr)
			conn.Write([]byte("you are out!\n"))
			message <- MakeMsg(client, "be out!")
			return
		case <-activeChan: // 活跃重置time.After计时器

		}
	}
}

func Manager() {
	// 初始化onlineMap
	onlineMap = make(map[string]Client)

	// 循环从msg中读取数据
	for {
		// 监听全局channel中是否有数据, 有数据存储至msg，无数据就阻塞
		msg := <-message

		// 循环发送消息给所有在线用户
		for _, client := range onlineMap {
			client.C <- msg
		}
	}

}
func main() {
	listener, err := net.Listen("tcp", "127.0.0.1:8000")
	if err != nil {
		fmt.Println("Listen err: ", err)
		return
	}
	defer listener.Close()

	// 创建管理者goroutine， 管理map和全局channel
	go Manager()

	// 循环监听客户端连接请求
	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("Accept err: ", err)
			return
		}

		// 启动go程处理客户端数据请求
		go HandleConnect(conn)
	}
	fmt.Println("end")
}
